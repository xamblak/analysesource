QT       += core
QT       -= gui

TARGET = AnalyseSources
TEMPLATE = app

SOURCES += main.cpp \
    parsearguments.cpp

HEADERS += \
    parsearguments.h

CONFIG += console
CONFIG += static

static {
    CONFIG += static
    DEFINES += STATIC
    message("Static build.")
}
